desc 'Show some demo outputs'
task 'boypoint:demo' => [:environment] do |_, args|
  demo_urls = ["https://forum.boypoint.de/post594126.html?hilit=halawa#p594127",
"https://forum.boypoint.de/post676190.html#p676191",
"https://forum.boypoint.de/post616698.html",
"https://forum.boypoint.de/topic21896.html",
"https://forum.boypoint.de/topic21896.html#p123",
"https://forum.boypoint.de/sex-f28/penislange-2-t17639-210.html",
"https://forum.boypoint.de/m-konferenzen-f53/10-2019-t23869.html",
"https://forum.boypoint.de/liebeskummer-f50/verliebt-dringend-hilfe-t20208-150.html#p644542",
"https://forum.boypoint.de/body-soul-f20/halwas-t12746.html?hilit=halawa",
"https://forum.boypoint.de/sex-f28/penislange-2-t17639-210.html",
"https://forum.boypoint.de/schwule-geschichten-f47/überschrift-t123.html",
"https://forum.boypoint.de/schwule-geschichten-f47/",
"https://forum.boypoint.de/member16714-posts.html",
"https://forum.boypoint.de/short/amazon.php/gp/product/B003BQM28I?camp=1638&creative=6742&creativeASIN=B003BQM28I&linkCode=as2&redirect=true&ref_=as_li_tf_tl&tag=bpauto-21"]

  demo_urls.each do |x|
    norm = boypoint_url_normalize(x);
    puts "bevor:" + x + "\nafter:" + norm + "\n";
    norm.scan(/forum.boypoint.de\/(view(topic|forum).php\?(t|f|p)=\d+)/).each do |scan_link|
      puts "found Link: #{scan_link[0]}";
    end
    puts;
  end
end

desc 'Replace all old phpBB-URLs with new Discourse-URLs'
task 'boypoint:remap' => [:environment] do |_, args|
  require 'highline/import'

  @boypoint_remap_debug_level = ask("Debug-Level? (default: 2) 1: only progress counter, 2: updated URLs, 3: verbose all infos").to_i;

  if @boypoint_remap_debug_level == 0 or @boypoint_remap_debug_level > 3
    @boypoint_remap_debug_level = 2;
  end

  posts = Post.where("raw ILIKE '%forum.boypoint.de/%'")
  posts_count_total = posts.count()
  i = 0
  total_links = 0;
  total_updates = 0;
  total_errors = 0;
  error_details = [];

  confirm_replace = ask("#{posts_count_total} passende Beiträge gefunden. Sollen alle überarbeitet werden? (Y/n)")
  exit 1 unless (confirm_replace == "" || confirm_replace.downcase == 'y')

  posts.each do |p|
    i += 1

    new_raw = boypoint_url_normalize(p.raw);

    debug("\nbefore normalize: #{p.raw}", 3);
    debug("after normalize: #{new_raw}", 3);

    new_raw.scan(/forum.boypoint.de\/(view(topic|forum).php\?(t|f|p)=\d+)/).each do |scan_link|
      total_links += 1;
      debug("\nfound Link: #{scan_link[0]}", 2);

      Permalink.where("url" => scan_link[0]).each do |link|
        replace = '';

        if link.post_id.present?
          debug("Post: #{link.post_id}", 2);
          replace = "forum.boypoint.de/p/#{link.post_id}";
        elsif link.topic_id.present?
          debug("Thread: #{link.topic_id}", 2);
          replace = "forum.boypoint.de/t/#{link.topic_id}";
        elsif  link.category_id.present?
          debug("Kategorie: #{link.category_id}", 2);
          replace = "forum.boypoint.de/c/#{link.category_id}";
        else
          debug("no Replacer found.", 2);
          next
        end

        debug("Replace: '#{replace}'", 2)
        new_raw = new_raw.gsub("forum.boypoint.de/#{link.url}", replace);
      end
    end

    if new_raw != p.raw
      begin
        total_updates += 1;
        debug("\nupdated Post: #{p.id}", 2);
        p.revise(Discourse.system_user, { raw: new_raw }, bypass_bump: true, skip_revision: true)
      rescue
        puts "\nFailed to remap post (topic_id: #{p.topic_id}, post_id: #{p.id})\n"
        error_details << "Failed to remap post (topic_id: #{p.topic_id}, post_id: #{p.id})";
        total_errors += 1;
      end
    end
    print_status(i, posts_count_total)
  end

  puts
  puts "\nErrors:";
  error_details.each { |detail| puts detail }
  puts "\nSummary: "
  puts "Total Posts with Links: #{posts_count_total}";
  puts "Total internal Links: #{total_links}";
  puts "Total updated Posts: #{total_updates}";
  puts "Total Errors: #{total_errors}";
  puts

end

def debug(output = "", required_level = 1)
  if @boypoint_remap_debug_level >= required_level
    puts output
  end
end

def boypoint_url_normalize(content)
  #TODO: add all normalize rules: https://forum.boypoint.de/t/seo-impact/41411/24
  #
  #    /post(\d+).html(.*)/viewtopic.php?p=\1
  #    /topic(\d+).html/viewtopic.php?t=\1
  #    /topic(\d+).html#p(\d+)/viewtopic.php?p=\2
  #    /(.*)-f(\d+)\/(.*)-t(\d+)(.*).html(.*)/viewtopic.php?t=\4
  #    /(.*)-f(\d+)/viewforum.php?f=\2
  #
  # => unhandled link structurs:
  #   https://forum.boypoint.de/sex-f28/penislange-2-t17639-210.html
  #   https://forum.boypoint.de/liebeskummer-f50/verliebt-dringend-hilfe-t20208.html#p644542

  content.gsub(/(.*)forum.boypoint.de\/(.*)#p(\d+)(.*)/,'\1forum.boypoint.de/viewtopic.php?p=\3\4')
         .gsub(/(.*)forum.boypoint.de\/(.*)-f(\d+)\/(.*)-t(\d+)-(\d+)(.*).html(.*)/,'\1forum.boypoint.de/viewtopic.php?t=\5/\6\7\8')
         .gsub(/(.*)forum.boypoint.de\/(.*)-f(\d+)\/(.*)-t(\d+)(.*).html(.*)/,'\1forum.boypoint.de/viewtopic.php?t=\5\6\7\8')
         .gsub(/(.*)forum.boypoint.de\/post(\d+).html(.*)/,'\1forum.boypoint.de/viewtopic.php?p=\2\3')
         .gsub(/(.*)forum.boypoint.de\/topic(\d+).html#p(\d+)(.*)/,'\1forum.boypoint.de/viewtopic.php?p=\3\4')
         .gsub(/(.*)forum.boypoint.de\/topic(\d+).html(.*)/,'\1forum.boypoint.de/viewtopic.php?t=\2\3')
         .gsub(/(.*)forum.boypoint.de\/(.*)-f(\d+)(.*)/,'\1forum.boypoint.de/viewforum.php?f=\3\4')

end
