# boypoint_url_cleaner

## INSTALL

1. Enter Discourse-Docker-Container (`./launcher enter app`)
2. `rake plugin:install[https://gitlab.com/nobodyfrm/boypoint_url_cleaner]`

## UNINSTALL
Delete Plugin Folder from Server:
1. Enter Discourse-Docker-Container (`./launcher enter app`)
2. `rm -rf plugins/boypoint_url_cleaner`

## USAGE
1. Enter Discourse-Docker-Container (`./launcher enter app`)
2. `rake boypoint:remap`
3. Answer two questions: 
   - Debug-Level: 1 only prgress counter, 2 (Default) updsted Links, 3 all details
   - Do it? (Y/n) show how much posts will be updated
4. wait! (drink coffee, count sheeps, read a book)

## DEMO
1. Enter Discourse-Docker-Container (`./launcher enter app`)
2. To check if all normalize rules working correctly, use `rake boypoint:demo`
